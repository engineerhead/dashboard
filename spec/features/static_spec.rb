require 'spec_helper'

describe "Static" do
  describe "GET /static" do
    it "works! (now write some real specs)" do
      # Run the generator again with the --webrat flag if you want to use webrat methods/matchers
      visit root_path
      page.should have_content( 'Find me' )
    end
  end
end


# describe "User Reg/Sign{in,out}" do
#   describe "sign up with email and password" do
#     it "allows users to register" do
#       visit new_user_registration_path
#       fill_in 'user_email', with: 'umair@gmail.comm'
#       fill_in 'user_password', with: 'password'
#       fill_in 'user_password_confirmation', with: 'password'
#       click_button 'Sign up'
#       page.should have_content( 'confirm your email' )
#     end
#   end
# end
