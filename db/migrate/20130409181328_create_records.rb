class CreateRecords < ActiveRecord::Migration
  def change
    create_table :records do |t|
      t.string :impressions
      t.string :ecpm
      t.string :revenue

      t.timestamps
    end
  end
end
