module RecordsHelper

	def is_admin?
		current_user.has_role? :admin
	end

	def get_all_users
		User.all
	end

	def get_total_impressions
		number_to_currency(@records.map {|x| x.impressions.to_i}.sum, unit: '', precision: 0)
	end

	def get_avg_ecpm
		number_to_currency(@records.map {|x| x.ecpm.to_f}.sum / @records.size)
	end

	def get_total_revenue
		number_to_currency(@records.map {|x| x.revenue.to_f}.sum)
	end
end
