$(function() {
  $( '.date_picker' ).datepicker({ 
  	"autoclose": true,
  	"format" : "dd/mm/yyyy"
  });

  var chart_data =  window.records;

   // ID of the element in which to draw the chart.
   if( chart_data && chart_data.length > 0 ){

   	var impressions = 0,
   			ecpm = 0,
   			revenue = 0 ;

   	$.each(chart_data, function( index, item){
   		impressions = impressions + parseInt(item.impressions);
   		ecpm = ecpm + parseFloat(item.ecpm);
   		revenue = revenue + parseFloat(item.revenue);
   	});

   	ecpmAvg = ecpm / chart_data.length;

   	$( '#impressions h1' ).text( impressions );
   	$( '#ecpm h1' ).text( '$' + ecpmAvg.toFixed(2) );
   	$( '#revenue h1' ).text( '$' + revenue.toFixed(2) );

    if( $( '#chart' ).length > 0 ){
      console.log('um');
  	  new Morris.Bar({
  	    element: 'chart',
  	    // Chart data records -- each entry in this array corresponds to a point on
  	    // the chart.
  	    data: chart_data,
  	    // The name of the data record attribute that contains x-values.
  	    xkey: 'date',
  	    // A list of names of data record attributes that contain y-values.
  	    ykeys: ['impressions', 'revenue'],
  	    // Labels for the ykeys -- will be displayed when you hover over the
  	    // chart.
  	    labels: ['Impressions', 'Revenue'],
  	    stacked: true
  	  });
    }
	}
});