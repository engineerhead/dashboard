class Record < ActiveRecord::Base
  attr_accessible :ecpm, :impressions, :revenue, :date, :user_id
  validates_presence_of :ecpm, :impressions, :revenue, :date, :user_id
  belongs_to :user

  def self.in_range( start_date, end_date)
    where( date: start_date..end_date ).order("date ASC")
  end
end
