class RecordsController < ApplicationController
  
  before_filter :authenticate_user!
  before_filter :only_allow_admin, only: [:new, :edit, :create, :update, :destroy]

  
  # GET /records
  # GET /records.json
  def index
    if params["/dashboard/records"].blank?
      @start_date = Date.today.beginning_of_month
      @end_date = Date.today
    else
      @start_date = Date.parse(params["/dashboard/records"][:start_date])
      @end_date = Date.parse(params["/dashboard/records"][:end_date])
      @user = User.find( params["/dashboard/records"][:user_id] ) unless params["/dashboard/records"][:user_id].blank?
    end
    
    if current_user.has_role? :admin
      if @user
        @records = @user.record.paginate( page: params[:page], per_page: 100).in_range( @start_date, @end_date )
      else
        @records = Record.paginate( page: params[:page], per_page: 100).in_range( @start_date, @end_date )
      end
    else
      @records = current_user.record.paginate( page: params[:page], per_page: 100).in_range( @start_date, @end_date )
    end

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @records }
    end
  end

  # GET /records/1
  # GET /records/1.json
  def show
    @record = Record.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @record }
    end
  end

  # GET /records/new
  # GET /records/new.json
  def new
    @record = Record.new
    @record.date = Date.today

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @record }
    end
  end

  # GET /records/1/edit
  def edit
    @record = Record.find(params[:id])
  end

  # POST /records
  # POST /records.json
  def create
    @record = Record.new(params[:record])

    respond_to do |format|
      if @record.save
        format.html { redirect_to new_record_url, notice: 'Record was successfully created.' }
        format.json { render json: @record, status: :created, location: @record }
      else
        format.html { render action: "new" }
        format.json { render json: @record.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /records/1
  # PUT /records/1.json
  def update
    @record = Record.find(params[:id])

    respond_to do |format|
      if @record.update_attributes(params[:record])
        format.html { redirect_to @record, notice: 'Record was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @record.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /records/1
  # DELETE /records/1.json
  def destroy
    @record = Record.find(params[:id])
    @record.destroy

    respond_to do |format|
      format.html { redirect_to records_url }
      format.json { head :no_content }
    end
  end

  def only_allow_admin
    redirect_to root_url, alert: 'Not authorized' unless current_user.has_role? :admin
  end
end
